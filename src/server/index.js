//Libraries that are imported to this Code
const csv = require ('csvtojson');
const fs = require ('fs');
const http = require ('http');
const port = 9877;

//Csv data and its path 
const  deliveriesFilePath  =  '../data/deliveries.csv';
const  matchesFilePath  =  '../data/matches.csv';


csv()
.fromFile ( matchesFilePath )
.then( ( matchData ) => { 
    
    ipl_Match_Played_Per_Year(matchData);
    matches_Won_By_Each_Team_Per_Year(matchData);
    
    csv()
    .fromFile ( deliveriesFilePath )
    .then( ( deliveriesData ) => { 

        extra_Runs_Conceded_By_Each_Team_In_The_Year( deliveriesData , id =startAndEndID( matchData , year =2016) ,year =2016);
        top_10_Economical_Bowler_In_The_Year( deliveriesData , id =startAndEndID( matchData , year =2015) ,year =2015);

    });
});

//Bellow function used to extract Match-Id of a particular Year 
function startAndEndID( matchData, year ){

    let start ;
    let end ;
    
    for(let Index = 0; Index < matchData.length; Index++){
    
        if( parseInt( matchData[ Index ].season)===year ){
    
            if( !start ){
    
                start=Index+1;
            }
      
            end=Index+1;
        }
    }
    return start+" "+end;
}


// Bellow function used to format the output data 
function output( folder_Name,answer )
{
    let str=JSON.stringify(answer , null , " ") 
    fs.writeFile('../output/'+folder_Name+'.json', str, function (err) { 
        if (err) {

        return console.log( "An Error Occurred while writing a file" );
        } 
    });

    if(folder_Name === "Top_10_Economical_Bowler_In_The_Year_2015"){

        webpage(port);
    }

}


// Bellow function is implemented to extract Number of Ipl match played per year
function ipl_Match_Played_Per_Year( matchData )
{
    let answer={};
    for(let Index = 0;  Index < matchData.length;  Index++){
        
        if(!answer[ matchData[ Index ].season ]){
        
            answer[ matchData[ Index ].season ] =1; 
        
        } else {

            answer[ matchData[ Index ].season ] +=1;
 
        }
    }
    output( 'IPL_Match_Played_Per_Year' , answer );
}

// Bellow function is implemented to extract Number of match won by each team per year
function matches_Won_By_Each_Team_Per_Year( matchData )
{
    let answer = {};
    for(let Index = 0;  Index < matchData.length;  Index++){
        
        if(!answer[ matchData[Index].season ]){
        
            answer[ matchData[Index].season ] = {};
            answer[ matchData[Index].season ][ matchData[Index].winner ] = 1;
        }
        else
        {
            if(!answer[ matchData[Index].season ][ matchData[Index].winner]){

                answer[ matchData[Index].season ][ matchData[Index].winner] = 1;
            } else {

                answer[ matchData[Index].season ][ matchData[Index].winner] += 1;
            }
        }
    }
    output( 'Matches_Won_By_Each_Team_Per_Year' , answer );
}

// Bellow function is implemented to extract extra runs conceded by each team in that year
function extra_Runs_Conceded_By_Each_Team_In_The_Year( deliveriesData , id , year ){
    
    let idValue = id.split(" ");
    let answer = {};

    for(let Index =0; Index < deliveriesData.length; Index++){
        
        if (parseInt( deliveriesData[ Index ].match_id ) >= parseInt( idValue[ 0 ] ) && parseInt( deliveriesData[ Index ].match_id ) <= parseInt( idValue[ 1 ]) ){
            
            if(answer[ deliveriesData[ Index ].bowling_team ] !== undefined){

                answer[ deliveriesData[ Index ].bowling_team ]+=parseInt( deliveriesData[ Index ].extra_runs );
            
            } else {
            
                answer[ deliveriesData[ Index ].bowling_team ]=parseInt( deliveriesData[ Index ].extra_runs );
            
            }
        }
    }
    output( 'Extra_Runs_Conceded_By_Each_Team_In_The_Year_'+year , answer );
}

// Bellow function is implemented to extract top 10 economical bowler in that year
function top_10_Economical_Bowler_In_The_Year( deliveriesData, id , year ){
    
    let idValue = id.split(" ");
    let answer = {}

    for(let Index =0; Index < deliveriesData.length; Index++){
       
        if (parseInt( deliveriesData[ Index ].match_id ) >= parseInt( idValue[ 0 ] ) && parseInt( deliveriesData[ Index ].match_id ) <= parseInt( idValue[ 1 ]) ){

            if(answer[ deliveriesData [ Index ].bowler ] === undefined ){

                    answer [ deliveriesData [Index].bowler ] = {
                    no_of_balls : 1,
                    score : parseInt( deliveriesData [ Index ].total_runs ) }
            } else {

                if(parseInt( deliveriesData [ Index ].wide_runs ) ===0 || parseInt( deliveriesData [ Index ].noball_runs ==0 )){

                    answer [ deliveriesData [ Index ].bowler ].no_of_balls += 1
                    answer [ deliveriesData [ Index ].bowler ].score += parseInt( deliveriesData [ Index ].total_runs ) 
                
                } else{
                
                    answer [ deliveriesData [ Index ].bowler ].no_of_balls += 0
                    answer [ deliveriesData [ Index ].bowler ].score += parseInt( deliveriesData [ Index ].total_runs )
                }
            }
        }
    }

    let allValue = {};
    let top10 = {};
    //Calculation of economic of the bowlers
    for( let key in answer )
    allValue [ key ] =( answer [ key ].score / ( answer [ key ].no_of_balls ) )*6;

    // Sorting the object using .sort 
    let key = Object.keys( allValue )
    key.sort( function ( value1, value2 ) 
        { return allValue [ value1 ] - allValue[ value2 ] });

    
    for( let Index =0; Index < 10; Index++ )
    top10[ key[ Index ] ] = allValue[ key[ Index ] ]

    output('Top_10_Economical_Bowler_In_The_Year_'+year,top10)
}


// Bellow function is implemented to read file
const promiseFileHandling = (path) =>{
   
    return new Promise((res , rej) =>{
        
        fs.readFile(path , (err , data) =>{
         
            if(err){
         
                console.log(err);
                rej(err);
         
            }
         
            res(data);
        })
    })
}

//Function implemented to create a server and listen to a port.
function webpage(port){
    
    const server = http.createServer((req , resp) => {
     
        switch(req.url) {
            case "/" : {

                promiseFileHandling('../client/index.html')
                .then((data) =>{

                    resp.writeHead(200 , {
                        'Content-Type' : 'text/html'
                    });
                    
                    resp.write(data);
                    resp.end();
                })
                .catch(err => {
                    
                    resp.writeHead(404 , {
                        'Content-Type' : 'text/html'
                    });
                    resp.write('404 not found');
                    resp.end();
                
                });
    
                break;
            }

            case "/app.js": {

                promiseFileHandling('../client/app.js')
                .then((data) =>{
                
                    resp.writeHead(200 , {
                        'Content-Type' : 'text/javascript' 
                    });
                
                    resp.write(data);
                    resp.end();
                })
                .catch(err => {
                
                    resp.writeHead(404 , {
                        'Content-Type' : 'text/html'
                    });
                    resp.write('404 not found');
                    resp.end();
                
                });

                break;
            }

            case "/app.css": {

                promiseFileHandling('../client/app.css')
                .then((data) =>{
                
                    resp.writeHead(200 , {
                        'Content-Type' : 'text/css' 
                    });
                
                    resp.write(data);
                    resp.end();
                })
                .catch(err => {
                
                    resp.writeHead(404 , {
                        'Content-Type' : 'text/html'
                    });
                    resp.write('404 not found');
                    resp.end();
                
                });

                break;
            }

            case "/Ipl_Match_Played_Per_Year": {
                promiseFileHandling("../output/IPL_Match_Played_Per_Year.json")
                .then((data) =>{

                    resp.writeHead(200 , {
                        'Content-Type' : 'application/json'
                    });
                    resp.write(data);
                    resp.end();
                })
                .catch(err => {
                    
                    resp.writeHead(404 , {
                        'Content-Type' : 'text/html'
                    });
                    resp.write('404 not found');
                    resp.end();

                });

                break;
            }


            case "/Matches_Won_By_Each_Team_Per_Year": {
                promiseFileHandling("../output/Matches_Won_By_Each_Team_Per_Year.json")
                .then((data) =>{

                    resp.writeHead(200 , {
                        'Content-Type' : 'application/json'
                    });
                    resp.write(data);
                    resp.end();
                })
                .catch(err => {

                    resp.writeHead(404 , {
                        'Content-Type' : 'text/html'
                    });
                    resp.write('404 not found');
                    resp.end();
                
                });

                break;
            }

            case "/Extra_Runs_Conceded_By_Each_Team_In_The_Year_2016": {
                
                promiseFileHandling("../output/Extra_Runs_Conceded_By_Each_Team_In_The_Year_2016.json")
                .then((data) =>{
                    
                    resp.writeHead(200 , {
                        'Content-Type' : 'application/json'
                    });
                    resp.write(data);
                    resp.end();
                
                })
                .catch(err => {
                
                    resp.writeHead(404 , {
                        'Content-Type' : 'text/html'
                    });
                    resp.write('404 not found');
                    resp.end();
                
                });

                break;
            }


            case "/Top_10_Economical_Bowler_In_The_Year_2015": {
                
                promiseFileHandling("../output/Top_10_Economical_Bowler_In_The_Year_2015.json")
                .then((data) =>{
                    
                    resp.writeHead(200 , {
                        'Content-Type' : 'application/json'
                    });
                
                    resp.write(data);
                    resp.end();
                
                })
                .catch(err => {
                
                    resp.writeHead(404 , {'Content-Type' : 'text/html'} );
                    resp.write('404 not found');
                    resp.end();
                
                });

                break;
            }

            default: {

                resp.writeHead(404 , {'Content-Type' : 'text/html'} );
                resp.write('404 not found');
                resp.end();
                break;

            }

        }
    })

    server.listen(port , function(err){

        if(err){
            
            console.log(err);
            console.log("Something went wrong at server listening port");
        } else {

            console.log("Server Listening port all good");
        }
    })

}
