let dataNeedToBeDisplayedGraphically = [
    "Ipl_Match_Played_Per_Year"  ,
    "Matches_Won_By_Each_Team_Per_Year"  ,
    "Extra_Runs_Conceded_By_Each_Team_In_The_Year_2016"  ,
    "Top_10_Economical_Bowler_In_The_Year_2015"  
]

for(let index = 0; index < 4; index++ ){

    fetch("/"+ dataNeedToBeDisplayedGraphically [index]+"")
    .then((resp) => {
  
        if( resp.ok === true ){
  
            return resp.json(); 
        } else {
  
            console.log('error');
        }
  
    })
    .then((data) => {
  
        if(dataNeedToBeDisplayedGraphically[ index ] === "Ipl_Match_Played_Per_Year"){
  
            Ipl_Match_Played_Per_Year(data);
  
        } else if(dataNeedToBeDisplayedGraphically[ index ] === "Matches_Won_By_Each_Team_Per_Year"){
  
            Matches_Won_By_Each_Team_Per_Year( Object.keys(data) , dataManipulation(data));
  
        } else if(dataNeedToBeDisplayedGraphically[ index ] === "Top_10_Economical_Bowler_In_The_Year_2015"){ 
          
            Top_10_Economical_Bowler_In_The_Year_2015(data);
  
        } else if(dataNeedToBeDisplayedGraphically[ index ] === "Extra_Runs_Conceded_By_Each_Team_In_The_Year_2016"){
  
            Extra_Runs_Conceded_By_Each_Team_In_The_Year_2016(data);
        }
  
    })
    .catch((error) => {
        console.log(error);
    });
  
  }



  function Ipl_Match_Played_Per_Year(data){

    //HighChart data to represent IPL matches played per year.

    Highcharts.chart('container2', {
        chart: {
          type: 'spline'
        },
        title: {
          text: "IPL Match played per year"
        },
        subtitle: {
          text: 'Source: Mount Blue.'
        },
        xAxis: {
          categories: Object.keys(data)                                        //Manipulated to requirement
        },
        yAxis: {
          title: {
            text: 'Number of Matches Played.'
          },
          labels: {
            formatter: function () {
              return this.value ;
            }
          }
        },
        tooltip: {
          crosshairs: true,
          shared: true
        },
        plotOptions: {
          spline: {
            marker: {
              radius: 4,
              lineColor: '#666666',
              lineWidth: 1
            }
          }
        },
        series: [{
          name: 'Matches Played ',
          marker: {
            symbol: 'square'
          },
          data: Object.values(data)                                        //Manipulated to requirement
      
        }]
    });

}


function Matches_Won_By_Each_Team_Per_Year( years , manipulatedData ){

    //HighChart data to represent Matches Won by each team per year.

    Highcharts.chart('container3', {
        chart: {
          type: 'column'
        },
        title: {
          text: 'Matches won by each Team per year'
        },
        subtitle: {
          text: 'Source: Mount Blue'
        },
        xAxis: {

          categories: years,                                        //Manipulated to requirement

          crosshair: true
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Winnings'
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
        },
        plotOptions: {
          column: {
            pointPadding: 0.2,
            borderWidth: 0
          }
        },

        series: manipulatedData ,                                        //Manipulated to requirement
      
    });
}

//Function to manipulate JSON file to requirements of HighChart code. 
function dataManipulation( data ){

    let year = Object.keys(data);
    let Answer ={}

    for(let Index =0; Index < year.length ; Index++ ){
   
        for(let Index1 =0; Index1 < Object.keys(data[ year[Index] ]).length; Index1++){
        
            //There is an issue with the title name of Rising Pune Supergiant so needed to have a extra operation for it. 
            if(Answer[ Object.keys(data[ year[Index] ])[Index1] ] === undefined && Object.keys(data[ year[Index] ])[Index1] !== "Rising Pune Supergiant"){
    
                Answer[ Object.keys(data[ year[Index] ])[Index1] ]= [];
                Answer[ Object.keys(data[ year[Index] ])[Index1] ].push(Object.values(data[ year[Index] ])[Index1]);
                
            } else if(Object.keys(data[ year[Index] ])[Index1] === "Rising Pune Supergiant"){

                Answer[ "Rising Pune Supergiants"].push(Object.values(data[ year[Index] ])[Index1]);
                                   
            } else {

                Answer[ Object.keys(data[ year[Index] ])[Index1] ].push(Object.values(data[ year[Index] ])[Index1]);
            }
        }
    }

    let Team = Object.keys(Answer);
    let Team_wins = Object.values(Answer);
    let Value =[];

    for( let Index = 0; Index < Team.length; Index++){

        let dummy = {
            name : Team[Index],
            data : Team_wins[Index]
        };

        Value.push(dummy);
    }

    return Value;
}


function Extra_Runs_Conceded_By_Each_Team_In_The_Year_2016( data ){

    //HighChart data to represent Extra Runs conceded by each team in the year 2016.
    Highcharts.chart('container4', {
        chart: {
          type: 'spline'
        },
        title: {
          text: "Extra runs conceded by each Team in 2016"
        },
        subtitle: {
          text: 'Source: Mount Blue.'
        },
        xAxis: {
          categories: Object.keys(data)                                        //Manipulated to requirement
        },
        yAxis: {
          title: {
            text: 'Runs.'
          },
          labels: {
            formatter: function () {
              return this.value ;
            }
          }
        },
        tooltip: {
          crosshairs: true,
          shared: true
        },
        plotOptions: {
          spline: {
            marker: {
              radius: 4,
              lineColor: '#666666',
              lineWidth: 1
            }
          }
        },
        series: [{
          name: 'Runs.',
          marker: {
            symbol: 'square'
          },
          data: Object.values(data)                                        //Manipulated to requirement
      
        }]
    });
}


function Top_10_Economical_Bowler_In_The_Year_2015( data ){

    //HighChart data to represent Top_10_Economical_Bowler_In_The_Year_2015.
    Highcharts.chart('container1', {
        chart: {
          type: 'column'
        },
        title: {
          text: 'Top 10 Economical Bowler In The Year 2015'
        },
        subtitle: {
          text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Mount Blue</a>'
        },
        xAxis: {
          type: 'category',
          labels: {
            rotation: -45,
            style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif'
            }
          }
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Percentage %'
          }
        },
        legend: {
          enabled: false
        },
        tooltip: {
          pointFormat: 'Economy Percentage in 2015: <b>{point.y:.1f}</b>'
        },
        series: [{
          name: '',
          data: Object.entries(data),                                        //Manipulated to requirement
          dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif'
            }
          }
        }]
    });
}